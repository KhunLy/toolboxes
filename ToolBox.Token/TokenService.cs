﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Text;

namespace ToolBox.Token
{
    public class TokenService : ITokenService
    {
        private readonly SymmetricSecurityKey _SecurityKey;
        private readonly JwtSecurityTokenHandler _Handler;
        private readonly string _Issuer;
        private readonly string _Audience;

        public TokenService()
        {
            _Handler = new JwtSecurityTokenHandler();
            _SecurityKey
                = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(ConfigurationManager.AppSettings.Get("Signature")));
            _Issuer = ConfigurationManager.AppSettings.Get("Issuer");
            _Audience = ConfigurationManager.AppSettings.Get("Audience");
        }

        public string GenerateToken<T>(T payload)
        {
            return this.GenerateToken<T>(payload, DateTime.UtcNow, DateTime.UtcNow.AddDays(1));
        }

        public string GenerateToken<T>(T payload, DateTime exp)
        {
            return this.GenerateToken<T>(payload, DateTime.UtcNow, exp);
        }

        public string GenerateToken<T>(T payload, DateTime nbf, DateTime exp)
        {
            SigningCredentials credentials = new SigningCredentials(_SecurityKey, SecurityAlgorithms.HmacSha256);

            JwtSecurityToken secToken = new JwtSecurityToken(
                _Issuer,
                _Audience,
                ToClaims(payload),
                nbf,
                exp,
                credentials
            );
            return _Handler.WriteToken(secToken);
        }

        public ClaimsPrincipal ValidateToken(string authToken)
        {
            TokenValidationParameters validationParameters = GetValidationParameters();
            return _Handler.ValidateToken(authToken, validationParameters, out SecurityToken validatedToken);
        }

        private TokenValidationParameters GetValidationParameters()
        {
            return new TokenValidationParameters()
            {
                ValidateLifetime = true,
                ValidateIssuer = true,
                ValidateAudience = false,
                ValidIssuer = _Issuer,
                RequireSignedTokens = true,
                IssuerSigningKey = _SecurityKey
            };
        }

        private IEnumerable<Claim> ToClaims<T>(T payload)
        {
            foreach (PropertyInfo prop in typeof(T).GetProperties().Where(p => p.CanRead && p.GetValue(payload) != null))
            {
                yield return new Claim(prop.Name, prop.GetValue(payload).ToString());
            }
        }
    }
}