﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.Mappers.Attributes;

namespace ToolBoxes.UnitTest.Mappers.Classes
{
    class Foo
    {
        public int Prop1 { get; set; }
        public string Prop2 { get; set; }
        public bool Prop3 { get; set; }
    }
    class Foo1
    {
        public int Prop1 { get; set; }
        public int Prop2 { get; set; }
        public bool Prop3 { get; set; }
    }

    class Foo2
    {
        [MapperIgnore]
        public int Prop1 { get; set; }
        public string Prop2 { get; set; }
    }

    class Foo3
    {
        [MapperProperty("OtherPropName")]
        public int Prop1 { get; set; }
    }
}
