﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.Mappers.Attributes;

namespace ToolBoxes.UnitTest.Mappers.Classes
{
    class FooDTO
    {
        public int Prop1 { get; set; }
        public string Prop2 { get; set; }
    }

    class Foo1DTO
    {
        public double Prop1 { get; set; }
        public string Prop2 { get; set; }
        public string Prop3 { get; set; }
    }

    class Foo2DTO
    {
        public int Prop1 { get; set; }
        [MapperIgnore]
        public string Prop2 { get; set; }
    }

    class Foo3DTO
    {
        public int OtherPropName { get; set; }
    }
}
