﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ToolBox.Mappers;
using ToolBoxes.UnitTest.Mappers.Classes;

namespace ToolBoxes.UnitTest.Mappers.UnitTests
{
    [TestClass]
    public class MapperExtensionsUnitTest
    {
        [TestMethod]
        public void SimpleMapToTest()
        {
            Foo foo = new Foo { Prop1 = 42, Prop2 = "Hello World !!", Prop3 = true };
            FooDTO fooDTO = foo.MapTo<FooDTO>();
            Assert.AreEqual(42, fooDTO.Prop1);
            Assert.AreEqual("Hello World !!", fooDTO.Prop2);
        }

        [TestMethod]
        public void ConvertPropMapToTest()
        {
            Foo1 foo = new Foo1 { Prop1 = 42, Prop2 = 1, Prop3 = true };
            Foo1DTO fooDTO = foo.MapTo<Foo1DTO>();
            Assert.AreEqual(42, fooDTO.Prop1);
            Assert.AreEqual("1", fooDTO.Prop2);
            Assert.AreEqual("True", fooDTO.Prop3);

            Foo1DTO foo1DTO = new Foo1DTO { Prop1 = 42.5, Prop2 = "1", Prop3 = "test" };
            Foo1 foo1 = foo1DTO.MapTo<Foo1>();
            Assert.AreEqual(42, foo1.Prop1);
            Assert.AreEqual(1, foo1.Prop2);
            Assert.AreEqual(false, foo1.Prop3);
        }

        [TestMethod]
        public void IgnorePropMapToTest()
        {
            Foo2 foo = new Foo2 { Prop1 = 42, Prop2 = "Hello World !!" };
            Foo2DTO fooDTO = foo.MapTo<Foo2DTO>();
            Assert.AreEqual(0, fooDTO.Prop1);
            Assert.AreEqual(null, fooDTO.Prop2);
        }

        [TestMethod]
        public void DifferentPropNameMapToTest()
        {
            Foo3 foo = new Foo3 { Prop1 = 42 };
            Foo3DTO fooDTO = foo.MapTo<Foo3DTO>();
            Assert.AreEqual(42, fooDTO.OtherPropName);
        }
    }
}
